const mix = require('laravel-mix');

const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
mix.webpackCongig = {
    plugins: [new BrowserSyncPlugin({

        host: 'localhost',
        port: 3000,
        server: {baseDir: ['public']}
    })
    ]
};


/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

// mix.js('resources/js/app.js', 'public/js')
//     .postCss('resources/css/app.css', 'public/css', [
//         //
//     ]);

mix.sass('resources/sass/style.scss', 'public/assets/css/')
    .copy('resources/fonts/*', 'public/assets/fonts/')


    .options({
        processCssUrls: false
    })
    .browserSync('coke_spake.com')
    .disableNotifications();
