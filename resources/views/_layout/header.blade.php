{{--<!doctype html>--}}
{{--<html lang="en">--}}

<head>
    <title>Coke Spark 2021</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}?v={{time()}}">
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '2430940943895617');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=2430940943895617&ev=PageView&noscript=1"
        /></noscript>
    <!-- End Facebook Pixel Code -->
</head>
<body >
<section    id="background-navigation" style="  background-image: url('/assets/images/background_header.png'); ">
    <div class="aaa">
        <header>
            {{--            <img  id="background-navigation" style=" width: 100%; height: 1100px;" src="assets/images/background_header.png">--}}
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="logococa" href="#">
                        <img src="/assets/images/logo_coca.png" alt="logo">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav justify-content-end" >
                            <li class="nav-item active">
                                <a class="nav-link" href="#">CƠ HỘI<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">VỊ TRÍ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">LỘ TRÌNH</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">QUY TRÌNH</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">TIÊU CHÍ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">RED TEAM</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <body>
        <div class="column1" >
            <a class="text_rucsacdo" >
                <img src="/assets/images/text_rucsacdo.png">
            </a>
            <div class="chuongtrinhtuyendung">CHƯƠNG TRÌNH TUYỂN DỤNG >>> </div>
            <div class="coke">COKE SPARK</div>
            <div class="textgiamsat">GIÁM SÁT BÁN HÀNG TIỀM NĂNG 2021</div>
            <div class="textmuonxongpha">      <div class="iconnext"> > </div>  Muốn xông pha thì đến với Coca-Cola</div>
        </div>
        <div class="column2">
            <a class="background_kimtuyen" href="#">
                <img style=" width: 80%; " src="/assets/images/background_kimtuyen.png">
            </a>
            <a class="models" href="#">
                <img style=" width: 90%;" src="/assets/images/img_models.png">
            </a>
        </div>

        {{--        TỰ TIN ỨNG TUYỂN, CƠ HỘI THỂ NGHIỆM--}}
        <div class="margintutin" style="padding-bottom: 600px">
            <h2 class="tutinungtuyen">TỰ TIN ỨNG TUYỂN, CƠ HỘI THỂ NGHIỆM</h2>
            <div class="container">
                <div class="text1" style="float: left">Tinh thần  <div class="text-style-1">"ĐẠI SỨ <br> THƯƠNG HIỆU"</div> cực chất <br>của  Coca-Cola</div>
                <img class="img1" src="/assets/images/img_model_first.png">
                <img class="img2" src="/assets/images/img_model_second.png">
                <div class="text2" >Bản lĩnh của <div class="text-style-1"><br>"NGƯỜI DẪN ĐẦU"</div> thực thụ</div>
                <div class="text3"> <div class="text-style-1">"TỎA CHẤT RIÊNG"</div> <br>trở thành phiên bản <br>  tuyệt nhất của chính mình </div>
                <img class="img3" src="/assets/images/img_model_third.png">
                <img class="img4"  src="/assets/images/img_model_fourth.png">
                <div class="text4" ><div class="text-style-1">"SỰ NGHIỆP ĐA QUỐC GIA"</div><br> ngay chính tại quê nhà</div>
            </div>
        </div>


        <div class="container">
            <img  class="backgroundvitrichat" src="/assets/images/background_vitrichat.png">
            <img class="teddyfirst"  src="/assets/images/teddy_first.png">

            <div class="vitri">VỊ TRÍ CHẤT <div class="textngang">-</div> <div class="textsunghiep">SỰ NGHIỆP PHẤT</div></div>
            {{--                <div class="vitri1">VỊ TRÍ CHẤT <div style="margin-top: 70px"> SỰ NGHIỆP PHẤT</div> </div>--}}
            <div class="bungchay">Bùng cháy sự nghiệp<div class="textbanhang">Bán hàng tại Coca-Cola</div></div>
            <p class="giamsat">Giám sát Bán hàng Tiềm năng</p>
            <img class="iconhome"  src="/assets/images/icon-home.png">
        </div>

        {{--LỘ TRÌNH SỰ NGHIỆP--}}

        <div class="lotrinh">  LỘ TRÌNH SỰ NGHIỆP <div class="textnhanh">NHANH CHÓNG,</div>
            <div class="textbanda" style="margin-top: 50px; margin-left: 50px;">BẠN ĐÃ SẴN SÀNG <div class="textbuctoc">BỨT TỐC?</div></div>
        </div>
        <div style="margin-top: 50px; margin-bottom: 200px; display: flex;" id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div style="width: 80%">
                <div class="carousel-item active" data-interval="200000">
                    <div class="columnlotrinh">
                        <a class="teddy2" >
                            <img style="width: 53%" src="/assets/images/teddy_second.png">
                        </a>
                    </div><br>
                    <div class="columnlotrinh">
                        <div class="textthang">6 THÁNG ĐẦU</div>
                        <div class="texthoanhap">Hòa nhập vào đội ngũ bán hàng, thực hiện nhiệm vụ một Nhân viên Bán hàng
                            với sự hỗ trợ huấn luyện bởi Quản lý Bán hàng khu vực/vùng.</div>
                    </div>
                </div>
                <div class="carousel-item" data-interval="200000">
                    <div class="columnlotrinh">
                        <a class="teddy2" >
                            <img style="width: 53%" src="/assets/images/teddy_third.png">
                        </a>
                    </div><br>
                    <div class="columnlotrinh">
                        <div class="textthang">6 THÁNG TIẾP THEO</div>
                        <div class="texthoanhap">Thực hiện nhiệm vụ Giám sát Bán hàng với sự kèm cặp của một Giám sát Bán hàng khác,
                            và vẫn được hỗ trợ huấn luyện bởi Quản lý Bán hàng khu vực/vùng</div>
                    </div>
                </div>
                <div class="carousel-item" data-interval="200000">
                    <div class="columnlotrinh">
                        <a class="teddy2" >
                            <img style="width: 53%" src="/assets/images/teddy_fourth.png">
                        </a>
                    </div><br>
                    <div class="columnlotrinh">
                        <div class="textthang">SAU 12 THÁNG</div>
                        <div class="texthoanhap">Tham gia đánh giá và trở thành Giám sát Bán hàng chính thức (nếu đạt yêu cầu)</div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <div class="container">
                    <img class="iconteddy" src="/assets/images/carousel_prev.png">
                </div>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <div class="container">
                    <img class="iconteddy2" src="/assets/images/carousel_next.png">
                </div>
            </a>
        </div>

        {{--        QUY TRÌNH ỨNG TUYỂN mobile--}}
        <div class="dislaycacbuoc" >
            <img class="imgquytrinh"  src="/assets/images/background_quytrinhungtuyen.png">
            <div class="container" style="padding-bottom: 0px">
                <h2 class="quytrinhungtuyen"> QUY TRÌNH ỨNG TUYỂN</h2>
                <h2 class="buocungtuyen"> CÁC BƯỚC ỨNG TUYỂN </h2>
                <div id="carouselExampleControls1" class="carousel slide" data-ride="carousel">
                    <div >
                        <div class="carousel-item active" data-interval="200000">
                            <div class="container">
                                <div>
                                    <a class="covermonth2">
                                        <img class="month" src="/assets/images/cover_month.png">
                                    </a>
                                    <p class="datemonth2" >Tháng 04 - 05</p>

                                </div>

                                <div class="textquytrinh1">Ứng tuyển và <br>sơ loại hồ sơ </div>
                            </div>
                        </div>
                        <div class="carousel-item" data-interval="200000">
                            <div class="container">
                                <div>
                                    <a class="covermonth2">
                                        <img class="month" src="/assets/images/cover_month.png">
                                    </a>
                                    <p class="datemonth2" >Tháng 06</p>
                                </div>
                                <div class="textquytrinh2" >Kiểm tra trực tuyến<div style="margin-left: 30px" > và Phỏng vấn</div></div>
                            </div>
                        </div>
                        <div class="carousel-item" data-interval="200000">
                            <div class="container">
                                <div>
                                    <a class="covermonth2">
                                        <img class="month" src="/assets/images/cover_month.png">
                                    </a>
                                    <p class="datemonth2" >Tháng 07</p>

                                </div>
                                <div class="textquytrinh3" > Gia nhập Coca-Cola <div style="margin-left: 100px"> Việt Nam</div></div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls1" role="button" data-slide="prev">
                        <div  class="container">
                            <img class="iconcarouselprevquytrinh" src="/assets/images/arow2.png">
                        </div>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls1" role="button" data-slide="next">
                        <div  class="container" >
                            <img class="iconcarouselprevquytrinh2"  src="/assets/images/arow.png">
                        </div>
                    </a>
                </div>
            </div>

            {{--       TIEU CHI TUYEN DUNG mobile--}}
            <div class="container" style="padding-bottom: 0px">
                <h2 class="quytrinhtd" >TIÊU CHÍ TUYỂN DỤNG</h2>
                <div id="carouselExampleControls2" class="carousel slide" data-ride="carousel">
                    <div >
                        <div class="carousel-item active" data-interval="200000">
                            <div class="container">
                                <div>
                                    <img class="icontieuchi1" src="./assets/images/icon_tieuchi_first.png">
                                </div>
                                <div class="texttieuchi">Sinh viên vừa tốt nghiệp Đại học với
                                    tối đa 1 năm kinh nghiệm làm việc.</div>
                            </div>
                        </div>
                        <div class="carousel-item" data-interval="200000">
                            <div class="container">
                                <div>
                                    <img class="icontieuchi1"  src="./assets/images/icon_tieuchi_second.png">
                                </div>
                                <div class="texttieuchi" >Điểm Đại học trung bình từ 6.0/10.0 trở lên hoặc tương đương.</div>
                            </div>
                        </div>
                        <div class="carousel-item" data-interval="200000">
                            <div class="container">
                                <div>
                                    <img class="icontieuchi1" src="./assets/images/icon_tieuchi_third.png">
                                </div>
                                <div class="texttieuchi"> Đam mê, nhiệt huyết, kiên trì, can đảm và khả năng học hỏi nhanh.</div>
                            </div>
                        </div>
                        <div class="carousel-item" data-interval="200000">
                            <div class="container">
                                <div>
                                    <img class="icontieuchi1" src="./assets/images/icon_tieuchi_fourth.png">
                                </div>
                                <div class="texttieuchi" >Có kỹ năng về giao tiếp, làm việc nhóm và giải quyết vấn đề là một ưu thế lớn.</div>
                            </div>
                        </div>
                        <div class="carousel-item" data-interval="200000">
                            <div class="container">
                                <div>
                                    <img class="icontieuchi1" src="./assets/images/icon_tieuchi_fifth.png">
                                </div>
                                <div class="texttieuchi" >Sẵn sàng chuyển đổi vùng làm việc theo yêu cầu của tổ chức.</div>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls2" role="button" data-slide="prev">
                        <div  class="container">
                            <img class="iconcarouselprevtuyendung1" src="/assets/images/arow2.png">
                        </div>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls2" role="button" data-slide="next">
                        <div  class="container" >
                            <img class="iconcarouselprevtuyendung2"  src="/assets/images/arow.png">
                        </div>
                    </a>
                </div>
            </div>
        </div>



        {{-------------------------------------------------------------------------------------}}
        {{--        QUY TRÌNH ỨNG TUYỂN laptop--}}
        <div class="dislayquytrinh">
            <img class="imgquytrinh"  src="/assets/images/background_quytrinhungtuyen.png">
            <div class="container" style="padding-bottom: 0px">
                <h2 class="quytrinhungtuyen"> QUY TRÌNH ỨNG TUYỂN</h2>
                <h2 class="buocungtuyen"> CÁC BƯỚC ỨNG TUYỂN </h2>
                <div >
                    <a class="covermonth" >
                        <img class="month"  src="/assets/images/cover_month.png">
                    </a>
                    <p class="datemonth">Tháng 04 - 05</p>
                    <a class="linemonth">
                        <img  src="/assets/images/line_month.png">
                    </a>
                    <a class="linequytrinh">
                        <img class="imglinequytring"  src="/assets/images/line_quytrinh.png">
                    </a>
                </div>
                <div>
                    <a class="covermonth2">
                        <img class="month" src="/assets/images/cover_month.png">
                    </a>
                    <p class="datemonth2" >Tháng 06</p>
                    <a class="linemonth1"  >
                        <img src="/assets/images/line_month.png">
                    </a>

                </div>
                <div>
                    <a class="covermonth3">
                        <img class="month" src="/assets/images/cover_month.png">
                    </a>
                    <p class="datemonth3">Tháng 07</p>
                    <a class="linemonth2" >
                        <img src="/assets/images/line_month.png">
                    </a>
                </div>
                <div class="textquytrinh">Ứng tuyển và <br>sơ loại hồ sơ </div>
                <div class="textquytrinh1">Kiểm tra trực tuyến<div style="margin-left: 30px" > và Phỏng vấn</div></div>
                <div class="textquytrinh2"> Gia nhập Coca-Cola <div style="margin-left: 70px"> Việt Nam</div></div>
                <h2 class="quytrinhtd" >TIÊU CHÍ TUYỂN DỤNG</h2>
                <img class="icontieuchi1" src="./assets/images/icon_tieuchi_first.png">
                <div class="texttieuchi">Sinh viên vừa tốt nghiệp Đại học với<br class="inline"> tối đa 1 năm kinh nghiệm làm việc.</div>
                <img class="icontieuchi4"  src="./assets/images/icon_tieuchi_second.png">
                <div class="texttieuchi1" >Điểm Đại học trung bình từ 6.0/10.0 trở lên hoặc tương đương.</div>
                <img class="icontieuchi5" src="./assets/images/icon_tieuchi_third.png">
                <div class="texttieuchi2" > Đam mê, nhiệt huyết, kiên trì,<br>  can đảm và khả năng học hỏi nhanh.</div>
                <img class="icontieuchi2"  src="./assets/images/icon_tieuchi_fourth.png">
                <div class="texttieuchi4"  >Có kỹ năng về giao tiếp, làm việc nhóm và giải quyết vấn đề là một ưu thế lớn.</div>
                <img class="icontieuchi6"  src="./assets/images/icon_tieuchi_fifth.png">
                <div class="texttieuchi5" >Sẵn sàng chuyển đổi vùng làm việc<br> theo yêu cầu của tổ chức.</div>

            </div>
        </div>



        {{--            RED TEAM--}}

        <div class="container" >
            <h2 class="textredteam">RED TEAM</h2>
        </div>
        <div style="margin-top: 50px; margin-bottom: 200px; " id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div >
                <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">
                    <div class="carousel-item active" data-interval="200000">
                        <div style=" margin-top: 400px" class="container">
                            <div class="redteam1">
                                <img class="imgredteam" src="/assets/images/img_redteam_first.png">
                                <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>
                                <div class="textcontent">"Mình bất đầu từ nhân viên bán hàng - vị trí cơ bản nhất, cũng là vị trí để lại cho mình nhiều bài học nhất. Dù đã chuẩn bị tâm lý trước, nhưng mình vẫn bị sốc khi vừa từ khách sạn hạng sang trở về địa bàn làm việc được công ty phân công tại Tri Tôn.
                                    Mình hầu như không biết gì về nơi mình đến, một mình không người thân. Nhưng thật may mắn,
                                    mình đã nhanh chóng hiểu được "chiến thôi"!
                                </div>

                                <img class="iconzoom1" src="/assets/images/zoom1.png">
                                <img class="iconzoom2" src="/assets/images/zoom2.png">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item" data-interval="200000">
                        <div style=" margin-top: 400px" class="container">
                            <div class="redteam1">
                                <img class="imgredteam" src="/assets/images/img_redteam_second.png">
                                <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>
                                <div  class="textcontent">"Thời gian thực chiến tại thị trường của Coke Spark thực sự đầy những
                                    thách thức. Lúc mới bắt đầu, khối lượng công việc hằng ngày khiến mình vô
                                    cùng bỡ ngỡ, không ngờ nhân viên sales mà phải xử lý bao nhiêu đó công
                                    việc, lại còn tiếp nhận cực nhiều thông tin. Đôi lúc mình cũng nản, đặc biệt
                                    là khi bị khách hàng phản đối. Nhưng lại càng hạnh phúc hơn khi được
                                    chính khách hàng đó tiếp nhận."</div>
                                <img class="iconzoom1" src="/assets/images/zoom1.png">
                                <img class="iconzoom2" src="/assets/images/zoom2.png">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item" data-interval="200000">
                        <div style=" margin-top: 400px" class="container">
                            <div class="redteam1">
                                <img class="imgredteam" src="/assets/images/img_redteam_third.png">
                                <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>
                                <div  class="textcontent1">“Điểm mình ấn tượng nhất tại Coca–Cola chính là ai cũng làm việc rất quyết
                                    liệt và không bao giờ từ bỏ mục tiêu đã đặt ra. Mình khắc ghi mãi câu truyền
                                    lửa của các anh cấp trên “Thay vì dành thời gian nghĩ cách xin sếp giảm chỉ
                                    tiêu, hãy suy nghĩ em cần làm gì, và cần hỗ trợ những gì để đạt được chỉ tiêu
                                    đó. Có thế mới trưởng thành và phá vỡ được những giới hạn của bản thân!”.
                                    Câu nói này cũng chính là kim chỉ nam giúp mình vượt qua được chính bản
                                    thân.”</div>
                                <img class="iconzoom1" src="/assets/images/zoom1.png">
                                <img class="iconzoom2" src="/assets/images/zoom2.png">
                            </div>
                        </div>
                    </div>
                    <div class="carousel-item" data-interval="200000">
                        <div style=" margin-top: 400px" class="container">
                            <div class="redteam1">
                                <img class="imgredteam" src="/assets/images/img_redteam_fourth.png">
                                <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>
                                <div  class="textcontent2">“Đến với Coke, bạn không phải lo ngại công việc nhàm chán. Đặc
                                    biệt, nếu bạn thích chu du đây đó thì Coke Spark thực sự là một môi
                                    trường làm việc lý tưởng.Ở giai đoạn đầu bạn sẽ được trau dồi và rèn dũa tất cả các kỹ năng
                                    bán hàng. Mình còn nhớ như in cảm giác vui tột cùng khi chính thức
                                    trở thành một giám sát tập sự sau khi kết thúc giai đoạn này. Đến
                                    giai đoạn thứ hai, bạn "oai hùng" trở thành người truyền lửa cho
                                    đồng nghiệp và nhân viên của mình. Thích hơn nữa chính là trải
                                    nghiệm điều đó ở một nơi khác.”</div>
                                <img class="iconzoom3" src="/assets/images/zoom1.png">
                                <img class="iconzoom4"  src="/assets/images/zoom2.png">
                            </div>
                        </div>
                    </div>
                </div>

                <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">
                    <div  class="container">
                        <img class="iconcarouselprev" src="/assets/images/carousel_prev.png">
                    </div>
                </a>
                <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">
                    <div  class="container" >
                        <img class="iconcarouselprev1"  src="/assets/images/carousel_next.png">
                    </div>
                </a>
            </div>
        </div>
        </body>


        <footer>
            <img class="imgfooter"  src="/assets/images/background_footer.png">
            <div class="container" style="padding-bottom: 1px;">
                <img class="icon" src="/assets/images/footer_fb.png">
                <img class="icon1"  src="/assets/images/footer_linkedin.png">
                <p class="textfooter">Copyright CokeLead © 2021.  All Rights Reserved</p>
            </div>
        </footer>
    </div>
</section>

