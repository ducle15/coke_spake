<!doctype html>
<html lang="en">
<head>
    <title>@yield('title')</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets/css/style.css')}}?v={{time()}}">
</head>
<body >
<section   id="background-navigation" style=" height: 1100px; background-image: url('/assets/images/background_header.png'); ">
    <div class="aaa">
        <header>
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="logococa" href="#">
                        <img src="/assets/images/logo_coca.png" alt="logo">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav justify-content-end" style="margin-left: 700px">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">CƠ HỘI<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">VỊ TRÍ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">LỘ TRÌNH</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">QUY TRÌNH</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">TIÊU CHÍ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">RED TEAM</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
        <body>
        <div class="column1" >
            <a class="text_rucsacdo" href="#">
                <img src="/assets/images/text_rucsacdo.png">
            </a>
            <div class="chuongtrinhtuyendung">CHƯƠNG TRÌNH TUYỂN DỤNG >> </div>
            <div class="coke">COKE SPARK</div>
            <div class="textgiamsat">GIÁM SÁT BÁN HÀNG TIỀM NĂNG 2021</div>
            <div class="textmuonxongpha">      <div class="iconnext"> > </div>  Muốn xông pha thì đến với Coca-Cola</div>
        </div>
        <div class="column2">
            <a class="background_kimtuyen" href="#">
                <img style=" width: 60%; " src="/assets/images/background_kimtuyen.png">
            </a>
            <a class="models" href="#">
                <img src="/assets/images/img_models.png">
            </a>
        </div>

        {{--        TỰ TIN ỨNG TUYỂN, CƠ HỘI THỂ NGHIỆM--}}
        <div style="padding-bottom: 600px">
            <h2 class="tutinungtuyen">TỰ TIN ỨNG TUYỂN, CƠ HỘI THỂ NGHIỆM</h2>
            <div class="container">
                <div class="text1" style="float: left">Tinh thần  <div class="text-style-1">"ĐẠI SỨ THƯƠNG HIỆU"</div> cực chất của Coca-Cola</div>
                <img class="img1" src="/assets/images/img_model_first.png">
                <img class="img2" src="/assets/images/img_model_second.png">
                <div class="text2" >Bản lĩnh của <div class="text-style-1">NGƯỜI DẪN ĐẦU</div> thực thụ</div>
                <div class="text3"> <div class="text-style-1">"TỎA CHẤT RIÊNG"</div> trở thành phiên bản tuyệt nhất của chính mình </div>
                <img class="img3" src="/assets/images/img_model_third.png">
                <img class="img4"  src="/assets/images/img_model_fourth.png">
                <div class="text4" ><div class="text-style-1">"SỰ NGHIỆP ĐA QUỐC GIA"</div> ngay chính tại que</div>
            </div>
        </div>

        {{--      <div >
                  </div>
                  <div class="tutinungtuyen">TỰ TIN ỨNG TUYỂN, CƠ HỘI THỂ NGHIỆM</div>
                  <div class="col1">
                      <div class="tinhthan" style="float: left; ">Tinh thần  <h2 class="text-style-1">"ĐẠI SỨ THƯƠNG HIỆU"</h2> cực chất của Coca-Cola</div>
                      <a class="imgmodel1" href="#">
                          <img  src="/assets/images/img_model_first.png">
                      </a>
                  </div>
                          column2
                  <div class="col1" >
                      <a class="imgmodel2" href="#">
                          <img src="/assets/images/img_model_second.png">
                      </a>
                      <div class="tinhthan" >Bản lĩnh của <div class="text-style-1">NGƯỜI DẪN ĐẦU</div> thực thụ</div>
                  </div><div style="color: #ffffff">.</div>
                          column3
                  <div class="col1">
                      <div class="toachatrieng"> <div class="text-style-1">"TỎA CHẤT RIÊNG"</div>, trở thành phiên bản tuyệt nhất của chính mình </div>
                      <a class="imgmodel1" href="#">
                          <img src="/assets/images/img_model_third.png">
                      </a>
                  </div>
                          column2
                  <div class="col1">
                      <a class="imgmodel2" href="#">
                          <img src="/assets/images/img_model_fourth.png">
                      </a>
                      <div class="tinhthan" style="float: left">Bản lĩnh của <div class="text-style-1">NGƯỜI DẪN ĐẦU</div> thực thụ</div>
                  </div>
                  <div>--}}
        <div>
            <div class="container">
                <img  class="backgroundvitrichat" src="/assets/images/background_vitrichat.png">
                <img class="teddyfirst"  src="/assets/images/teddy_first.png">

                <div class="vitri">VỊ TRÍ CHẤT - SỰ NGHIỆP PHẤT</div>
                <div class="bungchay">Bùng cháy sự nghiệp Bán hàng tại Coca - Cola</div>
                <p class="giamsat">Giám sát Bán hàng Tiềm năng</p>
                <img class="iconhome"  src="/assets/images/icon-home.png">
            </div>
        </div>

        {{--        LỘ TRÌNH SỰ NGHIỆP NHANH CHÓNG, BẠN ĐÃ SẴN SÀNG BỨC TỐC?--}}
        {{--        <div class="lotrinh">  LỘ TRÌNH SỰ NGHIỆP NHANH CHÓNG,<div style="margin-top: 70px; margin-left: 100px;">BẠN ĐÃ SẴN SÀNG BỨC TỐC?</div> </div>--}}
        {{--        <div class="columnlotrinh">--}}
        {{--            <a class="teddy2" href="#">--}}
        {{--                <img width=" width: 0%;" src="/assets/images/teddy_second.png">--}}
        {{--            </a>--}}
        {{--        </div><br>--}}
        {{--        <div class="columnlotrinh">--}}
        {{--            <div class="textthang">6 THÁNG ĐẦU</div>--}}
        {{--            <div class="texthoanhap">Hòa nhập vào đội ngũ bán hàng, thực hiện nhiệm vụ một Nhân viên Bán hàng--}}
        {{--                với sự hỗ trợ huấn luyện bởi Quản lý Bán hàng khu cực/vùng.</div>--}}
        {{--        </div>--}}
        {{--    <div class="container">--}}
        {{--        <img class="iconteddy" src="/assets/images/carousel_prev.png">--}}
        {{--        <img class="iconteddy2" style="left: 1600px;" src="/assets/images/carousel_next.png">--}}
        {{--    </div>--}}


        {{--    <div class="lotrinhsunghiep">--}}
        <div class="lotrinh">  LỘ TRÌNH SỰ NGHIỆP NHANH CHÓNG,<div style="margin-top: 70px; margin-left: 100px;">BẠN ĐÃ SẴN SÀNG BỨC TỐC?</div> </div>
        <div style="margin-top: 50px" id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="columnlotrinh">
                        <a class="teddy2" href="#">
                            <img width=" width: 0%;" src="/assets/images/teddy_second.png">
                        </a>
                    </div><br>
                    <div class="columnlotrinh">
                        <div class="textthang">6 THÁNG ĐẦU</div>
                        <div class="texthoanhap">Hòa nhập vào đội ngũ bán hàng, thực hiện nhiệm vụ một Nhân viên Bán hàng
                            với sự hỗ trợ huấn luyện bởi Quản lý Bán hàng khu cực/vùng.</div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="columnlotrinh">
                        <a class="teddy2" href="#">
                            <img width=" width: 0%;" src="/assets/images/teddy_third.png">
                        </a>
                    </div><br>
                    <div class="columnlotrinh">
                        <div class="textthang">6 THÁNG TIẾP THEO</div>
                        <div class="texthoanhap">Thực hiện nhiệm vụ Giám sát Bán hàng với sự kèm cặp chung với một Giám sát Bán hàng khác,
                            và vẫn được hỗ trợ huấn luyện bởi Quản lý Bán hàng khu vực/vùng</div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="columnlotrinh">
                        <a class="teddy2" href="#">
                            <img width=" width: 0%;" src="/assets/images/teddy_fourth.png">
                        </a>
                    </div><br>
                    <div class="columnlotrinh">
                        <div class="textthang">SAU 12 THÁNG</div>
                        <div class="texthoanhap">Tham gia đánh giá và trở thành Giám sát Bán hàng chính thức (nếu đạt yêu cầu)</div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
                <div class="container">
                    <img class="iconteddy" src="/assets/images/carousel_prev.png">
                </div>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
                <div class="container">
                    <img class="iconteddy2" style="left: 50px;" src="/assets/images/carousel_next.png">
                </div>
            </a>
        </div>
        {{--        QUY TRÌNH ỨNG TUYỂN--}}
        <img class="imgquytrinh" style="width: 100%; margin-top: 200px" src="/assets/images/background_quytrinhungtuyen.png">
        <div class="container" style="padding-bottom: 0px">
            <h2 class="quytrinhungtuyen"> QUY TRÌNH ỨNG TUYỂN</h2>
            <div >
                <a class="covermonth" href="#">
                    <img src="/assets/images/cover_month.png">
                </a>
                <p class="datemonth">Tháng 04-05</p>
                <a class="linemonth" href="#">
                    <img src="/assets/images/line_month.png">
                </a>
                <a class="linequytrinh" href="#">
                    <img style="width: 90%;" src="/assets/images/line_quytrinh.png">
                </a>
            </div>
            <div>
                <a class="covermonth" style="left: 400px;" href="#">
                    <img src="/assets/images/cover_month.png">
                </a>
                <p class="datemonth" style="left: 475px;">Tháng 06</p>
                <a class="linemonth " style="left: 530px;" href="#">
                    <img src="/assets/images/line_month.png">
                </a>
                {{--                    <a class="linequytrinh" href="#">--}}
                {{--                        <img src="/assets/images/line_quytrinh.png">--}}
                {{--                    </a>--}}
            </div>
            <div>
                <a class="covermonth" style="left: 1100px;" href="#">
                    <img src="/assets/images/cover_month.png">
                </a>
                <p class="datemonth" style="left: 1180px;">Tháng 07</p>
                <a class="linemonth " style="left: 1230px;" href="#">
                    <img src="/assets/images/line_month.png">
                </a>
                {{--                    <a class="linequytrinh" href="#">--}}
                {{--                        <img src="/assets/images/line_quytrinh.png">--}}
                {{--                    </a>--}}
            </div>
            <div class="textquytrinh" >Ứng tuyển và <div >sơ loại hồ sơ </div></div>
            <div class="textquytrinh" style="left: 300px;">Kiểm tra trực tuyến<div style="margin-left: 30px" > và Phỏng vấn</div></div>
            <div class="textquytrinh" style="left: 1030px; width: 100%"> Gia nhập Coca-Cola <div style="margin-left: 100px"> Việt Nam</div></div>
            <h2 class="quytrinhtd" >TIÊU CHÍ TUYỂN DỤNG</h2>
            <img class="icontieuchi1" src="./assets/images/icon_tieuchi_first.png">
            <div class="texttieuchi">Sinh viên vừa tốt nghiệp Đại học với tối đa 1 năm kinh nghiệm làm việc.</div>
            <img class="icontieuchi1" style=" bottom: 1200px;" src="./assets/images/icon_tieuchi_second.png">
            <div class="texttieuchi" style=" bottom: 1200px;">Điểm Đại học trung bình từ 6.0/100 trở lên hoặc tương đương.</div>
            <img class="icontieuchi1" style=" bottom: 1000px;" src="./assets/images/icon_tieuchi_third.png">
            <div class="texttieuchi" style=" bottom: 1000px;"> Đam mê, nhiệt huyết, kiên trì, can đảm và khả năng học hỏi nhanh.</div>
            <img class="icontieuchi1" style=" right: 440px; " src="./assets/images/icon_tieuchi_fourth.png">
            <div class="texttieuchi" style="  left: 800px;" >Có kỹ năng về giao tiếp, làm việc nhóm và giải quyết vấn đề là một ưu thế lớn.</div>
            <img class="icontieuchi1" style=" right: 440px;bottom: 1200px;" src="./assets/images/icon_tieuchi_fifth.png">
            <div class="texttieuchi" style="  left: 800px; bottom: 1200px;">Sẵn sàng chuyển đổi vùng làm việc theo yêu cầu của tổ chức.</div>
        </div>


        {{--        TIÊU CHÍ TUYỂN DỤNG--}}

        {{--        RED TEAM--}}
        <div style="margin-bottom: 200px" class="container">
            <h2 class="textredteam">RED TEAM</h2>
            <img class="imgredteam" src="/assets/images/img_redteam_first.png">
            <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>
            <div  class="textcontent">"Mình bất đầu từ nhân viên bán hàng - vị trí cơ bản nhất, cũng là vị trí để lại cho mình nhiều bài học nhất. Dù đã chuẩn bị tâm lý trước, nhưng mình vẫn bị sốc khi vừa từ khách sạn hạng sang trở về địa bàn làm việc được công ty phân công tại Tri Tôn.
                Mình hầu như không biết gì về nơi mình đến, một mình không người thân. Nhưng thật may mắn, mình đã nhanh chóng hiểu được "chiến thôi"!</div>
            <img class="iconcarouselprev" src="/assets/images/carousel_prev.png">
            <img class="iconcarouselprev" style="left: 1600px;" src="/assets/images/carousel_next.png">
            <img class="iconzoom1" src="/assets/images/zoom1.png">
            <img class="iconzoom2" src="/assets/images/zoom2.png">
        </div>
{{--                    <div >--}}
{{--                        <div class="container">--}}
{{--                            <h2 class="textredteam">RED TEAM</h2>--}}
{{--                        </div>--}}
{{--                        <div class="carousel-inner">--}}
{{--                            <div id="carouselExampleInterval" class="carousel slide" data-ride="carousel">--}}
{{--                                <div class="carousel-item active" data-interval="9000">--}}
{{--                                    <div style="margin-bottom: 400px ; margin-top: 400px" class="container">--}}

{{--                                        <img class="imgredteam" src="/assets/images/img_redteam_first.png">--}}
{{--                                        <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>--}}
{{--                                        <div  class="textcontent">"Mình bất đầu từ nhân viên bán hàng - vị trí cơ bản nhất, cũng là vị trí để lại cho mình nhiều bài học nhất. Dù đã chuẩn bị tâm lý trước, nhưng mình vẫn bị sốc khi vừa từ khách sạn hạng sang trở về địa bàn làm việc được công ty phân công tại Tri Tôn.--}}
{{--                                            Mình hầu như không biết gì về nơi mình đến, một mình không người thân. Nhưng thật may mắn, mình đã nhanh chóng hiểu được "chiến thôi"!</div>--}}
{{--                                        <img class="iconcarouselprev" src="/assets/images/carousel_prev.png">--}}
{{--                                        <img class="iconcarouselprev" style="left: 1600px;" src="/assets/images/carousel_next.png">--}}
{{--                                        <img class="iconzoom1" src="/assets/images/zoom1.png">--}}
{{--                                        <img class="iconzoom2" src="/assets/images/zoom2.png">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="carousel-item" data-interval="2000">--}}
{{--                                    <div style="margin-bottom: 400px ; margin-top: 400px" class="container">--}}

{{--                                        <img class="imgredteam" src="/assets/images/img_redteam_second.png">--}}
{{--                                        <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>--}}
{{--                                        <div  class="textcontent">"Thời gian thực chiến tại thị trường của Coke Spark thực sự đầy những--}}
{{--                                            thách thức. Lúc mới bắt đầu, khối lượng công việc hằng ngày khiến mình vô--}}
{{--                                            cùng bỡ ngỡ, không ngờ nhân viên sales mà phải xử lý bao nhiêu đó công--}}
{{--                                            việc, lại còn tiếp nhận cực nhiều thông tin. Đôi lúc mình cũng nản, đặc biệt--}}
{{--                                            là khi bị khách hàng phản đối. Nhưng lại càng hạnh phúc hơn khi được--}}
{{--                                            chính khách hàng đó tiếp nhận."</div>--}}
{{--                                        <img class="iconcarouselprev" src="/assets/images/carousel_prev.png">--}}
{{--                                        <img class="iconcarouselprev" style="left: 1600px;" src="/assets/images/carousel_next.png">--}}
{{--                                        <img class="iconzoom1" src="/assets/images/zoom1.png">--}}
{{--                                        <img class="iconzoom2" src="/assets/images/zoom2.png">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="carousel-item" data-interval="2000">--}}
{{--                                    <div style="margin-bottom: 400px ; margin-top: 400px" class="container">--}}

{{--                                        <img class="imgredteam" src="/assets/images/img_redteam_third.png">--}}
{{--                                        <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>--}}
{{--                                        <div  class="textcontent">“Điểm mình ấn tượng nhất tại Coca –Cola chính là ai cũng làm việc rất quyết--}}
{{--                                            liệt và không bao giờ từ bỏ mục tiêu đã đặt ra. Mình khắc ghi mãi câu truyền--}}
{{--                                            lửa của các anh cấp trên “Thay vì dành thời gian nghĩ cách xin sếp giảm chỉ--}}
{{--                                            tiêu, hãy suy nghĩ em cần làm gì, và cần hỗ trợ những gì để đạt được chỉ tiêu--}}
{{--                                            đó. Có thế mới trưởng thành và phá vỡ được những giới hạn của bản thân!”.--}}
{{--                                            Câu nói này cũng chính là kim chỉ nam giúp mình vượt qua được chính bản--}}
{{--                                            thân.”</div>--}}
{{--                                        <img class="iconcarouselprev" src="/assets/images/carousel_prev.png">--}}
{{--                                        <img class="iconcarouselprev" style="left: 1600px;" src="/assets/images/carousel_next.png">--}}
{{--                                        <img class="iconzoom1" src="/assets/images/zoom1.png">--}}
{{--                                        <img class="iconzoom2" src="/assets/images/zoom2.png">--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <div class="carousel-item" data-interval="2000">--}}
{{--                                    <div style="margin-bottom: 400px ; margin-top: 400px" class="container">--}}

{{--                                        <img class="imgredteam" src="/assets/images/img_redteam_fourth.png">--}}
{{--                                        <h4 class="textname">TÔ THÁI PHI</h4> <P class="textcoke" >Coke spark 2019</P>--}}
{{--                                        <div  class="textcontent">“Đến với Coke, bạn không phải lo ngại công việc nhàm chán. Đặc--}}
{{--                                            biệt, nếu bạn thích chu du đây đó thì Coke Spark thực sự là một môi--}}
{{--                                            trường làm việc lý tưởng.</div>--}}
{{--                                        <div  class="textcontent">--}}
{{--                                            Ở giai đoạn đầu bạn sẽ được trau dồi và rèn dũa tất cả các kỹ năng--}}
{{--                                            bán hàng. Mình còn nhớ như in cảm giác vui tột cùng khi chính thức--}}
{{--                                            trở thành một giám sát tập sự sau khi kết thúc giai đoạn này. Đến--}}
{{--                                            giai đoạn thứ hai, bạn "oai hùng" trở thành người truyền lửa cho--}}
{{--                                            đồng nghiệp và nhân viên của mình. Thích hơn nữa chính là trải--}}
{{--                                            nghiệm điều đó ở một nơi khác.”</div>--}}
{{--                                        <img class="iconcarouselprev" src="/assets/images/carousel_prev.png">--}}
{{--                                        <img class="iconcarouselprev" style="left: 1600px;" src="/assets/images/carousel_next.png">--}}
{{--                                        <img class="iconzoom1" src="/assets/images/zoom1.png">--}}
{{--                                        <img class="iconzoom2" src="/assets/images/zoom2.png">--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                            </div>--}}
{{--                            <a class="carousel-control-prev" href="#carouselExampleInterval" role="button" data-slide="prev">--}}
{{--                                <span class="carousel-control-prev-icon" aria-hidden="true"></span>--}}
{{--                                <span class="sr-only">Previous</span>--}}
{{--                            </a>--}}
{{--                            <a class="carousel-control-next" href="#carouselExampleInterval" role="button" data-slide="next">--}}
{{--                                <span class="carousel-control-next-icon" aria-hidden="true"></span>--}}
{{--                                <span class="sr-only">Next</span>--}}
{{--                            </a>--}}
{{--                        </div>--}}
{{--                    </div>--}}

        </body>
        <footer>
            <img class="imgfooter" style="width: 100%" src="/assets/images/background_footer.png">
            <div class="container">
                <img class="icon" src="/assets/images/footer_fb.png">
                <img class="icon" style="right: 300px;" src="/assets/images/footer_linkedin.png">
                <p class="textfooter">Copyright CokeLead © 2021.  All Rights Reserved</p>
            </div>
        </footer>
    </div>
</section>


<main>
    <section id="a-1"></section>
    <section id="a-2">
        <header>
            <div class="container-fluid">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <a class="logococa" href="#">
                        <img src="/assets/images/logo_coca.png" alt="logo">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav justify-content-end" style="margin-left: 700px">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">CƠ HỘI<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">VỊ TRÍ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">LỘ TRÌNH</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">QUY TRÌNH</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">TIÊU CHÍ</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link disabled" href="#">RED TEAM</a>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
        </header>
    </section>
    <section></section>
    <section></section>
    <section></section>
</main>
